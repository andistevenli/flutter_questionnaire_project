import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_questionnaire_project/features/about/about_screen.dart';
import 'package:flutter_questionnaire_project/features/dashboard/dashboard_controller.dart';
import 'package:flutter_questionnaire_project/features/dashboard/dashboard_screen.dart';
import 'package:flutter_questionnaire_project/features/forgot/forgot_screen.dart';
import 'package:flutter_questionnaire_project/features/login/login_screen.dart';
import 'package:flutter_questionnaire_project/features/opening/opening_screen.dart';
import 'package:flutter_questionnaire_project/features/profile/profile_controller.dart';
import 'package:flutter_questionnaire_project/features/profile/profile_screen.dart';
import 'package:flutter_questionnaire_project/features/register/register_screen.dart';
import 'package:flutter_questionnaire_project/features/questionnaire/preparation_screen.dart';
import 'package:flutter_questionnaire_project/features/questionnaire/questionnaire_controller.dart';
import 'package:flutter_questionnaire_project/features/questionnaire/questionnaire_screen.dart';
import 'package:flutter_questionnaire_project/features/providers/user_provider.dart';
import 'package:flutter_questionnaire_project/utils/constant/route.dart';
import 'package:flutter_questionnaire_project/utils/constant/text.dart';
import 'package:flutter_questionnaire_project/utils/key/navigator.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  runApp(
    const FlutterQuestionnaireProject(),
  );
}

class FlutterQuestionnaireProject extends StatelessWidget {
  const FlutterQuestionnaireProject({super.key});

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => DashboardController(),
        ),
        ChangeNotifierProvider(
          create: (context) => UserProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => QuestionnaireController(),
        ),
        ChangeNotifierProvider(
          create: (context) => ProfileController(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: projectName,
        theme: ThemeData.light(useMaterial3: true),
        navigatorKey: navigatorKey,
        initialRoute: openingScreen,
        routes: {
          forgotScreen: (context) => const ForgotScreen(),
          openingScreen: (context) => const OpeningScreen(),
          registerScreen: (context) => const RegisterScreen(),
          loginScreen: (context) => const LoginScreen(),
          dashboardRoute: (context) => const DashboardScreen(),
          profileRoute: (context) => const ProfileScreen(),
          aboutRoute: (context) => const AboutScreen(),
          preparationRoute: (context) => const PreparationScreen(),
          questionnaireRoute: (context) => const QuestionnaireScreen(),
        },
      ),
    );
  }
}
