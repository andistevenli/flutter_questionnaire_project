import 'package:flutter/material.dart';
import 'package:flutter_questionnaire_project/utils/constant/color.dart';

class LoadingStateWidget extends StatefulWidget {
  const LoadingStateWidget({super.key});

  @override
  State<LoadingStateWidget> createState() => _LoadingStateWidgetState();
}

class _LoadingStateWidgetState extends State<LoadingStateWidget> {
  @override
  Widget build(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator(
        color: secondary,
        strokeWidth: 2,
      ),
    );
  }
}
