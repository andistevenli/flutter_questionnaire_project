import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/login_model.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/user_model.dart';
import 'package:flutter_questionnaire_project/features/dashboard/dashboard_screen.dart';
import 'package:flutter_questionnaire_project/features/google/google_auth_helper.dart';
import 'package:flutter_questionnaire_project/features/providers/user_provider.dart';
import 'package:flutter_questionnaire_project/utils/constant/color.dart';
import 'package:flutter_questionnaire_project/utils/constant/route.dart';
import 'package:flutter_questionnaire_project/utils/constant/text.dart';
import 'package:flutter_questionnaire_project/utils/regex/regex.dart';
import 'package:flutter_questionnaire_project/utils/responsive/size_config.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _obscureText = true;

  FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final GoogleAuthHelper _googleAuthHelper = GoogleAuthHelper();
  UserModel userData = UserModel(
    name: '',
    npm: '',
    jurusan: '',
    dospem: '',
    email: '',
    password: '',
  );

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: primary005,
      body: Stack(
        children: [
          SafeArea(
            child: SingleChildScrollView(
              child: Container(
                padding: const EdgeInsets.all(40),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pushReplacementNamed(
                            context, '/register_screen');
                      },
                      child: Container(
                        alignment: Alignment.topRight,
                        child: const Text(
                          "Register",
                          style: TextStyle(
                            color: white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: getProportionateScreenHeight(80),
                    ),
                    const Text(
                      "Login",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 32,
                          color: white),
                    ),
                    SizedBox(
                      height: getProportionateScreenHeight(18),
                    ),
                    Form(
                      child: Column(
                        children: [
                          Container(
                            height: getProportionateScreenHeight(40),
                            width: SizeConfig.screenWidth,
                            decoration: BoxDecoration(
                                color: white,
                                borderRadius: BorderRadius.circular(50)),
                            child: TextFormField(
                              keyboardType: TextInputType.visiblePassword,
                              textInputAction: TextInputAction.next,
                              onChanged: (value) {},
                              controller: _emailController,
                              decoration: const InputDecoration(
                                contentPadding: EdgeInsets.all(16),
                                border: InputBorder.none,
                                hintText: "Email",
                                hintStyle: TextStyle(color: grey),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: getProportionateScreenHeight(18),
                          ),
                          Container(
                            height: getProportionateScreenHeight(40),
                            width: SizeConfig.screenWidth,
                            decoration: BoxDecoration(
                                color: white,
                                borderRadius: BorderRadius.circular(50)),
                            child: TextFormField(
                              obscureText: _obscureText,
                              textInputAction: TextInputAction.next,
                              onChanged: (value) {},
                              controller: _passwordController,
                              decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.all(16),
                                  border: InputBorder.none,
                                  hintText: "Password",
                                  hintStyle: const TextStyle(color: grey),
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        _obscureText = !_obscureText;
                                      });
                                    },
                                    icon: Icon(
                                      _obscureText
                                          ? Icons.visibility
                                          : Icons.visibility_off,
                                      color: black,
                                    ),
                                  )),
                            ),
                          ),
                          SizedBox(height: getProportionateScreenHeight(10)),
                          InkWell(
                            onTap: () {
                              Navigator.pushNamed(context, '/forgot_screen');
                            },
                            child: Container(
                              alignment: Alignment.centerRight,
                              child: const Text(
                                forgotPassword,
                                style: TextStyle(
                                  color: white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: getProportionateScreenHeight(15)),
                          SizedBox(
                            height: getProportionateScreenHeight(40),
                            width: SizeConfig
                                .screenWidth, // Set the width as needed
                            child: ElevatedButton(
                              onPressed: () async {
                                if (_emailController.text.isEmpty ||
                                    _passwordController.text.isEmpty) {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      content: Text(
                                          "Email dan Password harus diisi"),
                                      backgroundColor: red,
                                    ),
                                  );
                                } else if (!RegExp(emailRegex)
                                    .hasMatch(_emailController.text)) {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      content: Text("Email Invalid"),
                                      backgroundColor: red,
                                    ),
                                  );
                                } else if (!RegExp(passwordRegex)
                                    .hasMatch(_passwordController.text)) {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      content: Text("Password Invalid"),
                                      backgroundColor: red,
                                    ),
                                  );
                                } else {
                                  try {
                                    LoginModel login = LoginModel(
                                      email: _emailController.text,
                                      password: _passwordController.text,
                                    );
                                    await context
                                        .read<UserProvider>()
                                        .loginUser(login);
                                    User? firebaseUser =
                                        firebaseAuth.currentUser;

                                    if (firebaseUser != null) {
                                      // ignore: use_build_context_synchronously
                                      userData = await context
                                          .read<UserProvider>()
                                          .getUserInfo(_emailController.text);
                                      // ignore: use_build_context_synchronously
                                      Navigator.pushNamedAndRemoveUntil(
                                        context,
                                        dashboardRoute,
                                        (route) => false,
                                        arguments: DashboardArgument(
                                            userModel: userData),
                                      );
                                    }
                                  } catch (error) {
                                    // ignore: use_build_context_synchronously
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        content: Text(
                                            "Terjadi Kesalahan saat Login: $error"),
                                        backgroundColor: red,
                                      ),
                                    );
                                  }
                                }
                              },
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: secondary),
                              child: const Text(
                                "Login",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: getProportionateScreenHeight(55),
                    ),
                    Image.asset(
                      "assets/images/line_login.png",
                    ),
                    const SizedBox(
                      height: 15.0,
                    ),
                    SizedBox(
                      height: getProportionateScreenHeight(40),
                      width: SizeConfig.screenWidth, // Set the width as needed
                      child: ElevatedButton(
                          onPressed: () async {
                            User? user =
                                await _googleAuthHelper.signInWithGoogle();
                            if (user != null) {
                              // ignore: use_build_context_synchronously
                              userData = await context
                                  .read<UserProvider>()
                                  .getUserInfo(user.email!);
                              // ignore: use_build_context_synchronously
                              Navigator.pushNamedAndRemoveUntil(
                                context,
                                dashboardRoute,
                                (route) => false,
                                arguments:
                                    DashboardArgument(userModel: userData),
                              );
                            }
                          },
                          style: ElevatedButton.styleFrom(
                              backgroundColor: secondary),
                          child: Row(
                            children: [
                              Image.network(
                                "https://i.ibb.co/7WBNQ3M/281764.png",
                                height: getProportionateScreenHeight(30),
                                width: getProportionateScreenWidth(30),
                              ),
                              SizedBox(
                                width: getProportionateScreenWidth(10),
                              ),
                              const Text(
                                "Lanjutkan dengan Google",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(
                                width: getProportionateScreenWidth(10),
                              ),
                              Icon(
                                Icons.arrow_forward,
                                color: black,
                                size: getProportionateScreenHeight(15),
                              )
                            ],
                          )),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
