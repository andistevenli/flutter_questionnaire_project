import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/google_user_model.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/user_model.dart';
import 'package:flutter_questionnaire_project/features/dashboard/dashboard_screen.dart';
import 'package:flutter_questionnaire_project/features/google/google_auth_helper.dart';
import 'package:flutter_questionnaire_project/features/providers/user_provider.dart';
import 'package:flutter_questionnaire_project/utils/constant/color.dart';
import 'package:flutter_questionnaire_project/utils/constant/route.dart';
import 'package:flutter_questionnaire_project/utils/regex/regex.dart';
import 'package:flutter_questionnaire_project/utils/responsive/size_config.dart';
import 'package:provider/provider.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({super.key});

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  bool _obscureText = true;

  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _npmController = TextEditingController();
  final TextEditingController _jurusanController = TextEditingController();
  final TextEditingController _dospemController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final GoogleAuthHelper _googleAuthHelper = GoogleAuthHelper();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: primary005,
      body: Stack(
        children: [
          SafeArea(
            child: SingleChildScrollView(
              child: Container(
                padding: const EdgeInsets.all(40),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pushReplacementNamed(
                            context, '/login_screen');
                      },
                      child: Container(
                        alignment: Alignment.topRight,
                        child: const Text(
                          "Log In",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: getProportionateScreenHeight(40),
                    ),
                    const Text(
                      "Register",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 32,
                          color: white),
                    ),
                    SizedBox(
                      height: getProportionateScreenHeight(15),
                    ),
                    Form(
                      child: Column(
                        children: [
                          Container(
                            height: getProportionateScreenHeight(40),
                            width: SizeConfig.screenWidth,
                            decoration: BoxDecoration(
                                color: white,
                                borderRadius: BorderRadius.circular(50)),
                            child: TextFormField(
                              textInputAction: TextInputAction.next,
                              onChanged: (value) {},
                              controller: _nameController,
                              decoration: const InputDecoration(
                                contentPadding: EdgeInsets.all(16),
                                border: InputBorder.none,
                                hintText: "Nama lengkap",
                                hintStyle: TextStyle(color: grey),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: getProportionateScreenHeight(5),
                          ),
                          Container(
                            height: getProportionateScreenHeight(40),
                            width: SizeConfig.screenWidth,
                            decoration: BoxDecoration(
                                color: white,
                                borderRadius: BorderRadius.circular(50)),
                            child: TextFormField(
                              textInputAction: TextInputAction.next,
                              onChanged: (value) {},
                              controller: _npmController,
                              decoration: const InputDecoration(
                                contentPadding: EdgeInsets.all(16),
                                border: InputBorder.none,
                                hintText: "NPM",
                                hintStyle: TextStyle(color: grey),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: getProportionateScreenHeight(5),
                          ),
                          Container(
                            height: getProportionateScreenHeight(40),
                            width: SizeConfig.screenWidth,
                            decoration: BoxDecoration(
                                color: white,
                                borderRadius: BorderRadius.circular(50)),
                            child: TextFormField(
                              textInputAction: TextInputAction.next,
                              onChanged: (value) {},
                              controller: _jurusanController,
                              decoration: const InputDecoration(
                                contentPadding: EdgeInsets.all(16),
                                border: InputBorder.none,
                                hintText: "Jurusan",
                                hintStyle: TextStyle(color: grey),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: getProportionateScreenHeight(5),
                          ),
                          Container(
                            height: getProportionateScreenHeight(40),
                            width: SizeConfig.screenWidth,
                            decoration: BoxDecoration(
                                color: white,
                                borderRadius: BorderRadius.circular(50)),
                            child: TextFormField(
                              textInputAction: TextInputAction.next,
                              onChanged: (value) {},
                              controller: _dospemController,
                              decoration: const InputDecoration(
                                contentPadding: EdgeInsets.all(16),
                                border: InputBorder.none,
                                hintText: "Dosen pebimbing",
                                hintStyle: TextStyle(color: grey),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: getProportionateScreenHeight(5),
                          ),
                          Container(
                            height: getProportionateScreenHeight(40),
                            width: SizeConfig.screenWidth,
                            decoration: BoxDecoration(
                                color: white,
                                borderRadius: BorderRadius.circular(50)),
                            child: TextFormField(
                              textInputAction: TextInputAction.next,
                              onChanged: (value) {},
                              controller: _emailController,
                              decoration: const InputDecoration(
                                contentPadding: EdgeInsets.all(16),
                                border: InputBorder.none,
                                hintText: "Email",
                                hintStyle: TextStyle(color: grey),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: getProportionateScreenHeight(5),
                          ),
                          Container(
                            height: getProportionateScreenHeight(40),
                            width: SizeConfig.screenWidth,
                            decoration: BoxDecoration(
                                color: white,
                                borderRadius: BorderRadius.circular(50)),
                            child: TextFormField(
                              textInputAction: TextInputAction.next,
                              onChanged: (value) {},
                              obscureText: _obscureText,
                              controller: _passwordController,
                              decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.all(16),
                                  border: InputBorder.none,
                                  hintText: "Password",
                                  hintStyle: const TextStyle(color: grey),
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        _obscureText = !_obscureText;
                                      });
                                    },
                                    icon: Icon(
                                      _obscureText
                                          ? Icons.visibility
                                          : Icons.visibility_off,
                                      color: black,
                                    ),
                                  )),
                            ),
                          ),
                          SizedBox(height: getProportionateScreenHeight(10)),
                          SizedBox(
                            height: getProportionateScreenHeight(40),
                            width: SizeConfig.screenWidth,
                            child: ElevatedButton(
                              onPressed: () async {
                                if (_emailController.text.isEmpty ||
                                    _passwordController.text.isEmpty ||
                                    _dospemController.text.isEmpty ||
                                    _jurusanController.text.isEmpty ||
                                    _npmController.text.isEmpty ||
                                    _nameController.text.isEmpty) {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      content: Text("Semua Field harus diisi"),
                                      backgroundColor: red,
                                    ),
                                  );
                                } else if (!RegExp(emailRegex)
                                    .hasMatch(_emailController.text)) {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      content: Text("Email Invalid"),
                                      backgroundColor: red,
                                    ),
                                  );
                                } else if (!RegExp(passwordRegex)
                                    .hasMatch(_passwordController.text)) {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      content: Text("Password Invalid"),
                                      backgroundColor: red,
                                    ),
                                  );
                                } else {
                                  try {
                                    UserModel user = UserModel(
                                      name: _nameController.text,
                                      npm: _npmController.text,
                                      jurusan: _jurusanController.text,
                                      dospem: _dospemController.text,
                                      email: _emailController.text,
                                      password: _passwordController.text,
                                    );

                                    // Register user with FirebaseAuth
                                    await context
                                        .read<UserProvider>()
                                        .registerUser(user);

                                    // Check if registration is successful by verifying the current user
                                    User? firebaseUser =
                                        FirebaseAuth.instance.currentUser;
                                    if (firebaseUser != null) {
                                      // ignore: use_build_context_synchronously
                                      Navigator.pushNamedAndRemoveUntil(
                                        context,
                                        dashboardRoute,
                                        (route) => false,
                                        arguments:
                                            DashboardArgument(userModel: user),
                                      );
                                    } else {
                                      // ignore: use_build_context_synchronously
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        const SnackBar(
                                          content: Text(
                                              "Gagal mendaftarkan pengguna."),
                                          backgroundColor: red,
                                        ),
                                      );
                                    }
                                  } catch (error) {
                                    // Tangani kesalahan lainnya
                                    // ignore: use_build_context_synchronously
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        content: Text(
                                            'Terjadi kesalahan saat mendaftarkan pengguna: $error'),
                                        backgroundColor: red,
                                      ),
                                    );
                                  }
                                }
                              },
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: secondary),
                              child: const Text(
                                "Register",
                                style: TextStyle(
                                  color: black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 15.0,
                    ),
                    Image.asset(
                      "assets/images/line_login.png",
                    ),
                    const SizedBox(
                      height: 15.0,
                    ),
                    SizedBox(
                      height: getProportionateScreenHeight(40),
                      width: SizeConfig.screenWidth,
                      child: ElevatedButton(
                          onPressed: () async {
                            GoogleUserModel? googleUser =
                                await _googleAuthHelper.registerWithGoogle();
                            if (googleUser != null) {
                              _nameController.text = googleUser.name;
                              _emailController.text = googleUser.email;
                            }
                          },
                          style: ElevatedButton.styleFrom(
                              backgroundColor: secondary),
                          child: Row(
                            children: [
                              Image.network(
                                "https://i.ibb.co/7WBNQ3M/281764.png",
                                height: getProportionateScreenHeight(30),
                                width: getProportionateScreenWidth(30),
                              ),
                              SizedBox(
                                width: getProportionateScreenWidth(10),
                              ),
                              const Text(
                                "Lanjutkan dengan Google",
                                style: TextStyle(
                                  color: black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(
                                width: getProportionateScreenWidth(10),
                              ),
                              Icon(
                                Icons.arrow_forward,
                                color: black,
                                size: getProportionateScreenHeight(15),
                              )
                            ],
                          )),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
