import 'package:flutter/material.dart';
import 'package:flutter_questionnaire_project/utils/constant/color.dart';
import 'package:flutter_questionnaire_project/utils/extensions/white_space.dart';

class SubinfoWidget extends StatefulWidget {
  final String label;
  final String value;

  const SubinfoWidget({
    required this.label,
    required this.value,
    super.key,
  });

  @override
  State<SubinfoWidget> createState() => _SubinfoWidgetState();
}

class _SubinfoWidgetState extends State<SubinfoWidget> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Text(
              widget.label,
              style: const TextStyle(
                color: white,
                fontSize: 16,
              ),
            ),
          ),
          3.0.dp,
          Container(
            alignment: Alignment.centerLeft,
            height: 48.0,
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            color: secondary,
            child: Text(
              widget.value,
              style: const TextStyle(
                color: black,
                fontSize: 16,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
