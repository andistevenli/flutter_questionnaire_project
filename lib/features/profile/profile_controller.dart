import 'package:flutter/material.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/user_model.dart';
import 'package:flutter_questionnaire_project/core/domain/use-case/profile_interactor.dart';
import 'package:flutter_questionnaire_project/utils/state/finite_state.dart';

class ProfileController with ChangeNotifier {
  final ProfileInteractor profileInteractor = ProfileInteractor();
  late UserModel userModel;

  FiniteState _finiteState = FiniteState.initial;
  FiniteState get finiteState => _finiteState;

  void changeState(FiniteState finiteState) {
    _finiteState = finiteState;
    notifyListeners();
  }

  Future<void> getProfileInfo(final String npm) async {
    try {
      changeState(FiniteState.loading);
      userModel = await profileInteractor.getProfileInfo(npm);
      changeState(FiniteState.loaded);
    } catch (e) {
      changeState(FiniteState.failed);
      throw Exception(e);
    }
  }

  Future<void> userLogout() async {
    try {
      await profileInteractor.userLogout();
    } catch (e) {
      throw Exception(e);
    }
  }
}
