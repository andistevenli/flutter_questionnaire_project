import 'package:flutter/material.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/user_model.dart';
import 'package:flutter_questionnaire_project/features/profile/profile_controller.dart';
import 'package:flutter_questionnaire_project/features/profile/widgets/subinfo_widget.dart';
import 'package:flutter_questionnaire_project/utils/constant/color.dart';
import 'package:flutter_questionnaire_project/utils/constant/route.dart';
import 'package:flutter_questionnaire_project/utils/constant/text.dart';
import 'package:flutter_questionnaire_project/utils/constant/unit.dart';
import 'package:flutter_questionnaire_project/utils/extensions/white_space.dart';
import 'package:flutter_questionnaire_project/utils/responsive/size_config.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  late ProfileController profileController;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      profileController =
          Provider.of<ProfileController>(context, listen: false);
      final ProfileArgument profileArgs =
          ModalRoute.of(context)!.settings.arguments as ProfileArgument;
                profileController.getProfileInfo(profileArgs.userModel.npm);
    });
  }

  @override
  Widget build(BuildContext context) {
    final ProfileArgument args = ModalRoute.of(context)!.settings.arguments as ProfileArgument;
    return Scaffold(
      backgroundColor: primary005,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(screenPadding),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: IconButton(
                  iconSize: getProportionateScreenWidth(24.0),
                  onPressed: () => Navigator.pop(context),
                  icon: const Icon(
                    Icons.arrow_back,
                    color: white,
                  ),
                ),
              ),
              25.0.dp,
              Expanded(
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Column(
                    children: [
                      const Text(
                        profile,
                        style: TextStyle(
                          color: white,
                          fontWeight: FontWeight.bold,
                          fontSize: 24,
                        ),
                      ),
                      14.0.dp,
                      Image.asset(
                        'assets/images/profile_bg.png',
                        width: getProportionateScreenWidth(128.0),
                      ),
                      14.0.dp,
                      Column(
                        children: [
                          SubinfoWidget(
                            label: fullName,
                            value: args.userModel.name,
                          ),
                          9.0.dp,
                          SubinfoWidget(
                            label: idNumber,
                            value: args.userModel.npm,
                          ),
                          9.0.dp,
                          SubinfoWidget(
                            label: major,
                            value: args.userModel.jurusan,
                          ),
                          9.0.dp,
                          SubinfoWidget(
                            label: advisor,
                            value: args.userModel.dospem,
                          ),
                          9.0.dp,
                          SubinfoWidget(
                            label: email,
                            value: args.userModel.email,
                          ),
                        ],
                      ),
                      22.0.dp,
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: EdgeInsets.only(left: 8),
                          child: Text(
                            more,
                            style: TextStyle(
                              color: white,
                              fontSize: 16,
                            ),
                          ),
                        ),
                      ),
                      11.0.dp,
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: InkWell(
                            onTap: () =>
                                Navigator.pushNamed(context, aboutRoute),
                            child: const Text(
                              about,
                              style: TextStyle(
                                color: white,
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ),
                      ),
                      11.0.dp,
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: InkWell(
                            onTap: () => showModalBottomSheet(
                              context: context,
                              builder: (context) => Container(
                                color: white,
                                width: double.infinity,
                                child: Padding(
                                  padding: const EdgeInsets.all(screenPadding),
                                  child: Wrap(
                                    children: [
                                      const Text(
                                        confirmQuestion,
                                        style: TextStyle(
                                          color: black,
                                          fontSize: 16.0,
                                        ),
                                      ),
                                      25.76.dp,
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              backgroundColor: secondary,
                                              elevation: 0.0,
                                              fixedSize: Size(
                                                getProportionateScreenWidth(
                                                    121),
                                                getProportionateScreenHeight(
                                                    32.86),
                                              ),
                                            ),
                                            onPressed: () =>
                                                Navigator.pop(context),
                                            child: const Text(
                                              no,
                                              style: TextStyle(
                                                color: black,
                                                fontSize: 16,
                                              ),
                                            ),
                                          ),
                                          const SizedBox(
                                            width: 25.0,
                                          ),
                                          ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              backgroundColor: red,
                                              elevation: 0.0,
                                              fixedSize: Size(
                                                getProportionateScreenWidth(
                                                    121),
                                                getProportionateScreenHeight(
                                                    32.86),
                                              ),
                                            ),
                                            onPressed: () {
                                              profileController.userLogout();
                                              Navigator.pushNamedAndRemoveUntil(
                                                  context,
                                                  openingScreen,
                                                  (route) => false);
                                            },
                                            child: const Text(
                                              yes,
                                              style: TextStyle(
                                                color: white,
                                                fontSize: 16,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            child: const Text(
                              logout,
                              style: TextStyle(
                                color: red,
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ProfileArgument {
  final UserModel userModel;

  ProfileArgument({
    required this.userModel,
  });
}
