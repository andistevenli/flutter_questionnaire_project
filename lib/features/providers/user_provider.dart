import 'package:flutter/material.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/forgot_model.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/login_model.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/user_model.dart';
import 'package:flutter_questionnaire_project/core/data/source/remote/firebase/firebase_services.dart';

class UserProvider extends ChangeNotifier {
  final FirebaseService firebaseService = FirebaseService();

  Future<void> registerUser(UserModel user) async {
    await firebaseService.addUserToFirebase(user);
    await firebaseService.userSignInFirebase(user);
    notifyListeners();
  }

  Future<void> loginUser(LoginModel login) async {
    await firebaseService.userLoginFirebase(login);
    notifyListeners();
  }

  Future<void> forgotUser(ForgotModel forgot) async {
    await firebaseService.userForgotFirebase(forgot);
    notifyListeners();
  }

  Future<UserModel> getUserInfo(final String email) async {
    return await firebaseService.getUserInfo(email);
  }
}
