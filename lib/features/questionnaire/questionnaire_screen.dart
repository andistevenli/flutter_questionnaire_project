import 'package:flutter/material.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/result_model.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/user_model.dart';
import 'package:flutter_questionnaire_project/features/dashboard/dashboard_screen.dart';
import 'package:flutter_questionnaire_project/features/questionnaire/C4.5-algorithm/c45_algorithm.dart';
import 'package:flutter_questionnaire_project/features/questionnaire/questionnaire_controller.dart';
import 'package:flutter_questionnaire_project/features/widgets/empty_state_widget.dart';
import 'package:flutter_questionnaire_project/features/widgets/error_state_widget.dart';
import 'package:flutter_questionnaire_project/features/widgets/loading_state_widget.dart';
import 'package:flutter_questionnaire_project/utils/constant/color.dart';
import 'package:flutter_questionnaire_project/utils/constant/route.dart';
import 'package:flutter_questionnaire_project/utils/constant/text.dart';
import 'package:flutter_questionnaire_project/utils/constant/unit.dart';
import 'package:flutter_questionnaire_project/utils/extensions/white_space.dart';
import 'package:flutter_questionnaire_project/utils/responsive/size_config.dart';
import 'package:flutter_questionnaire_project/utils/state/finite_state.dart';
import 'package:provider/provider.dart';

class QuestionnaireScreen extends StatefulWidget {
  const QuestionnaireScreen({super.key});

  @override
  State<QuestionnaireScreen> createState() => _QuestionnaireScreenState();
}

class _QuestionnaireScreenState extends State<QuestionnaireScreen> {
  late final QuestionnaireController questionnaireController;

  final List<int> _selectedValueOption =
      List.generate(25, (index) => 0, growable: false);

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      questionnaireController =
          Provider.of<QuestionnaireController>(context, listen: false);

      questionnaireController.getQuestionnaireInfo();
      questionnaireController.questionnaireAnswerValue =
          List.generate(25, (index) => 3, growable: false);
      questionnaireController.questionnaireAnswerLabel =
          List.generate(25, (index) => 'Netral', growable: false);
    });
  }

  @override
  Widget build(BuildContext context) {
    final QuestionnaireArgument args =
        ModalRoute.of(context)!.settings.arguments as QuestionnaireArgument;
    return Scaffold(
      backgroundColor: primary005,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(screenPadding),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: IconButton(
                  iconSize: getProportionateScreenWidth(24.0),
                  onPressed: () => Navigator.pop(context),
                  icon: const Icon(
                    Icons.arrow_back,
                    color: white,
                  ),
                ),
              ),
              34.0.dp,
              const Text(
                questionnaire,
                style: TextStyle(
                  color: white,
                  fontWeight: FontWeight.bold,
                  fontSize: 24,
                ),
              ),
              50.0.dp,
              Expanded(
                child: Consumer<QuestionnaireController>(
                  builder: (context, controller, _) {
                    if (controller.finiteState == FiniteState.loading) {
                      return const LoadingStateWidget();
                    } else if (controller.finiteState == FiniteState.failed) {
                      return const ErrorStateWidget();
                    } else {
                      if (controller.questionnaireContent.isEmpty) {
                        return const EmptyStateWidget();
                      } else {
                        return ListView.separated(
                          itemBuilder: (context, index) {
                            return Container(
                              padding: const EdgeInsets.all(16),
                              alignment: Alignment.topLeft,
                              decoration: BoxDecoration(
                                color: white,
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    '${controller.questionnaireContent[index].number.toString()}. ',
                                    style: const TextStyle(
                                      color: black,
                                      fontSize: 14,
                                    ),
                                  ),
                                  Expanded(
                                    child: Column(
                                      children: [
                                        Text(
                                          controller.questionnaireContent[index]
                                              .question,
                                          style: const TextStyle(
                                            color: black,
                                            fontSize: 14,
                                          ),
                                        ),
                                        Column(
                                          children: List.generate(
                                            controller
                                                .questionnaireContent[index]
                                                .optionsModel
                                                .length,
                                            (optionIndex) => Row(
                                              children: [
                                                Radio(
                                                  value: controller
                                                      .questionnaireContent[
                                                          index]
                                                      .optionsModel[optionIndex]
                                                      .value,
                                                  groupValue:
                                                      _selectedValueOption[
                                                          index],
                                                  onChanged: (value) {
                                                    setState(() {
                                                      _selectedValueOption[
                                                          index] = value!;
                                                      controller
                                                              .questionnaireAnswerValue[
                                                          index] = value;
                                                      controller.questionnaireAnswerLabel[
                                                              index] =
                                                          controller
                                                              .questionnaireContent[
                                                                  index]
                                                              .optionsModel[
                                                                  optionIndex]
                                                              .content;
                                                    });
                                                  },
                                                ),
                                                Expanded(
                                                  child: Text(
                                                    controller
                                                        .questionnaireContent[
                                                            index]
                                                        .optionsModel[
                                                            optionIndex]
                                                        .content,
                                                    style: const TextStyle(
                                                      color: black,
                                                      fontSize: 14,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                          separatorBuilder: (context, index) => 6.0.dp,
                          itemCount: controller.questionnaireContent.length,
                        );
                      }
                    }
                  },
                ),
              ),
              11.0.dp,
              Align(
                alignment: Alignment.centerLeft,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: secondary,
                    elevation: 1.0,
                    fixedSize: Size(
                      getProportionateScreenWidth(90),
                      getProportionateScreenHeight(34),
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                  onPressed: () {
                    showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (context) => AlertDialog(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0.0)),
                        titleTextStyle: const TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                          color: white,
                        ),
                        contentTextStyle: const TextStyle(
                          fontSize: 14,
                          color: white,
                        ),
                        backgroundColor: primary005,
                        title: const Text('Kuesioner'),
                        content: const Text(confirmQuestion),
                        actions: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: secondary,
                                  elevation: 0.0,
                                  fixedSize: Size(
                                    getProportionateScreenWidth(102),
                                    getProportionateScreenHeight(39),
                                  ),
                                ),
                                onPressed: () => Navigator.pop(context),
                                child: const Text(
                                  back,
                                  style: TextStyle(
                                    color: black,
                                    fontSize: 16,
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 33.0,
                              ),
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: secondary,
                                  elevation: 0.0,
                                  fixedSize: Size(
                                    getProportionateScreenWidth(102),
                                    getProportionateScreenHeight(39),
                                  ),
                                ),
                                onPressed: () {
                                  List<String> features = [
                                    'Tangible',
                                    'Reliability',
                                    'Responsiveness',
                                    'Assurance',
                                    'Emphaty',
                                  ];
                                  List<List<int>> answers = [
                                    questionnaireController
                                        .questionnaireAnswerValue,
                                  ];
                                  DecisionTree decisionTree = DecisionTree();
                                  Node root = decisionTree.buildDecisionTree(
                                      answers, features);
                                  Map<String, double> satisfactionPercentages =
                                      decisionTree
                                          .calculateSatisfactionPercentage(
                                              answers[0], root, features);
                                  questionnaireController.postResult(
                                    ResultModel(
                                      npm: args.userModel.npm,
                                      respondentName: args.userModel.name,
                                      satisfactionPercentage:
                                          satisfactionPercentages[
                                                  'Satisfaction'] ??
                                              0.0,
                                      dissatisfactionPercentage:
                                          satisfactionPercentages[
                                                  'Dissatisfaction'] ??
                                              0.0,
                                      answersValue: questionnaireController
                                          .questionnaireAnswerValue,
                                      answersLabel: questionnaireController
                                          .questionnaireAnswerLabel,
                                    ),
                                  );
                                  Navigator.pushNamedAndRemoveUntil(
                                    context,
                                    dashboardRoute,
                                    (route) => false,
                                    arguments: DashboardArgument(userModel: args.userModel,),
                                  );
                                },
                                child: const Text(
                                  sure,
                                  style: TextStyle(
                                    color: black,
                                    fontSize: 16,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    );
                  },
                  child: const Text(
                    submitButton,
                    style: TextStyle(
                      color: black,
                      fontSize: 13,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class QuestionnaireArgument {
  final UserModel userModel;

  QuestionnaireArgument({
    required this.userModel,
  });
}
