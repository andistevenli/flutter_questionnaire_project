import 'package:flutter/material.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/questionnaire_model.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/result_model.dart';
import 'package:flutter_questionnaire_project/core/domain/use-case/questionnaire_interactor.dart';
import 'package:flutter_questionnaire_project/utils/state/finite_state.dart';

class QuestionnaireController with ChangeNotifier {
  final QuestionnaireInteractor questionnaireInteractor =
      QuestionnaireInteractor();
  late List<QuestionnaireModel> questionnaireContent = [];
  late List<int> questionnaireAnswerValue;
  late List<String> questionnaireAnswerLabel;

  FiniteState _finiteState = FiniteState.initial;
  FiniteState get finiteState => _finiteState;

  void changeState(FiniteState finiteState) {
    _finiteState = finiteState;
    notifyListeners();
  }

  Future<void> getQuestionnaireInfo() async {
    try {
      changeState(FiniteState.loading);
      questionnaireContent =
          await questionnaireInteractor.getQuestionnaireInfo();
      changeState(FiniteState.loaded);
    } catch (e) {
      changeState(FiniteState.failed);
      throw Exception(e);
    }
  }

  Future<void> postResult(ResultModel resultModel) async {
    try {
      questionnaireInteractor.postResult(resultModel);
    } catch (e) {
      throw Exception(e);
    }
  }
}
