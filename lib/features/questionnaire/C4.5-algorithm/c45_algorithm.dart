import 'dart:math';

class Node {
  String? label;
  String? feature;
  Map<String, Node>? children;

  Node({this.label, this.feature, this.children});
}

class DecisionTree {
  double calculateEntropy(List<int> labels) {
    Set<int> uniqueLabels = Set<int>.from(labels);
    double entropy = 0.0;

    for (int label in uniqueLabels) {
      double labelProbability =
          labels.where((l) => l == label).length / labels.length;
      entropy -= labelProbability *
          (labelProbability == 0
              ? 0
              : (labelProbability * log(labelProbability) / ln2));
    }

    return entropy;
  }

  String getMajorityLabel(List<int> labels) {
    Map<int, int> labelCounts = {};

    for (int label in labels) {
      labelCounts[label] = (labelCounts[label] ?? 0) + 1;
    }

    int majorityLabel =
        labelCounts.entries.reduce((a, b) => a.value > b.value ? a : b).key;

    return majorityLabel.toString();
  }

  String chooseBestFeature(List<List<int>> answers, List<String> features) {
    double initialEntropy =
        calculateEntropy(answers.map((answer) => answer.last).toList());

    double maxInfoGain = 0.0;
    String bestFeature = '';

    for (String feature in features) {
      int featureIndex = features.indexOf(feature);
      Set<int> uniqueValues =
          Set<int>.from(answers.map((answer) => answer[featureIndex]));

      double newEntropy = 0.0;

      for (int value in uniqueValues) {
        List<int> subset = [];
        for (int i = 0; i < answers.length; i++) {
          if (answers[i][featureIndex] == value) {
            subset.add(answers[i].last);
          }
        }
        double subsetWeight = subset.length / answers.length;
        newEntropy += subsetWeight * calculateEntropy(subset);
      }

      double infoGain = initialEntropy - newEntropy;

      if (infoGain > maxInfoGain) {
        maxInfoGain = infoGain;
        bestFeature = feature;
      }
    }

    return bestFeature;
  }

  Node buildDecisionTree(List<List<int>> answers, List<String> features) {
    if (answers.isEmpty) {
      return Node(label: 'No Data');
    }

    if (answers.every((label) => label.last == answers[0].last)) {
      return Node(label: answers[0].last.toString());
    }

    if (features.isEmpty) {
      String majorityLabel =
          getMajorityLabel(answers.map((answer) => answer.last).toList());
      return Node(label: majorityLabel);
    }

    String bestFeature = chooseBestFeature(answers, features);

    Node currentNode = Node(feature: bestFeature);
    int bestFeatureIndex = features.indexOf(bestFeature);
    Set<int> uniqueValues =
        Set<int>.from(answers.map((answer) => answer[bestFeatureIndex]));

    for (int value in uniqueValues) {
      List<List<int>> subset = [];
      List<String> newFeatures = List.from(features);
      newFeatures.remove(bestFeature);

      for (int i = 0; i < answers.length; i++) {
        if (answers[i][bestFeatureIndex] == value) {
          subset.add(answers[i]);
        }
      }
      Node childNode = buildDecisionTree(subset, newFeatures);
      currentNode.children ??= {};
      currentNode.children![value.toString()] = childNode;
    }

    return currentNode;
  }

  String predict(List<int> answer, Node decisionTree, List<String> features) {
    if (decisionTree.children == null || decisionTree.children!.isEmpty) {
      return decisionTree.label!;
    }

    String feature = decisionTree.feature!;
    int featureIndex = features.indexOf(feature);

    String value = answer[featureIndex].toString();

    if (!decisionTree.children!.containsKey(value)) {
      return decisionTree.label!;
    }

    Node childNode = decisionTree.children![value]!;
    return predict(answer, childNode, features);
  }

  Map<String, double> calculateSatisfactionPercentage(
      List<int> answer, Node decisionTree, List<String> features) {
    String predictedLabel = predict(answer, decisionTree, features);

    if (predictedLabel == '1') {
      return {'Satisfaction': 0.0, 'Dissatisfaction': 100.0};
    } else if (predictedLabel == '5') {
      return {'Satisfaction': 100.0, 'Dissatisfaction': 0.0};
    } else {
      double satisfactionPercentage = double.parse(predictedLabel) * 20.0;
      double dissatisfactionPercentage = 100.0 - satisfactionPercentage;

      return {
        'Satisfaction': satisfactionPercentage,
        'Dissatisfaction': dissatisfactionPercentage
      };
    }
  }
}
