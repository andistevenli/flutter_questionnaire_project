import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/google_user_model.dart';
import 'package:google_sign_in/google_sign_in.dart';

class GoogleAuthHelper {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<User?> signInWithGoogle() async {
    try {
      GoogleSignInAccount? googleUser = await _googleSignIn.signIn();
      if (googleUser != null) {
        GoogleSignInAuthentication googleAuth = await googleUser.authentication;
        AuthCredential credential = GoogleAuthProvider.credential(
          accessToken: googleAuth.accessToken,
          idToken: googleAuth.idToken,
        );
        UserCredential userCredential =
            await _auth.signInWithCredential(credential);
        User? user = userCredential.user;
        return user;
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }

  Future<GoogleUserModel?> registerWithGoogle() async {
    try {
      GoogleSignInAccount? googleUser = await _googleSignIn.signIn();

      if (googleUser != null) {
        String name = googleUser.displayName ?? "";
        String email = googleUser.email;

        GoogleUserModel user = GoogleUserModel(
          name: name,
          email: email,
        );

        return user;
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }
}
