import 'package:flutter/material.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/forgot_model.dart';
import 'package:flutter_questionnaire_project/features/providers/user_provider.dart';
import 'package:flutter_questionnaire_project/utils/constant/color.dart';
import 'package:flutter_questionnaire_project/utils/constant/text.dart';
import 'package:flutter_questionnaire_project/utils/regex/regex.dart';
import 'package:flutter_questionnaire_project/utils/responsive/size_config.dart';
import 'package:provider/provider.dart';

class ForgotScreen extends StatefulWidget {
  const ForgotScreen({super.key});

  @override
  State<ForgotScreen> createState() => _ForgotScreenState();
}

class _ForgotScreenState extends State<ForgotScreen> {
  final TextEditingController _emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: primary005,
      appBar: AppBar(
        backgroundColor: primary005,
        iconTheme: const IconThemeData(color: white),
        actions: const [],
      ),
      body: Stack(
        children: [
          SafeArea(
            child: SingleChildScrollView(
              child: Container(
                padding: const EdgeInsets.all(40),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Image.asset(
                      "assets/images/forgot_password.png",
                    ),
                    SizedBox(
                      height: getProportionateScreenHeight(30),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: const Text(
                        forgotPassword,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 24,
                            color: white),
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: const Text(
                        forgotDescription,
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            fontSize: 13,
                            color: white),
                      ),
                    ),
                    SizedBox(
                      height: getProportionateScreenHeight(18),
                    ),
                    Form(
                      child: Column(
                        children: [
                          Container(
                            height: getProportionateScreenHeight(40),
                            width: SizeConfig.screenWidth,
                            decoration: BoxDecoration(
                                color: white,
                                borderRadius: BorderRadius.circular(50)),
                            child: TextFormField(
                              keyboardType: TextInputType.visiblePassword,
                              textInputAction: TextInputAction.next,
                              onChanged: (value) {},
                              controller: _emailController,
                              decoration: const InputDecoration(
                                contentPadding: EdgeInsets.all(16),
                                border: InputBorder.none,
                                hintText: "Alamat Email",
                                hintStyle: TextStyle(color: grey),
                              ),
                            ),
                          ),
                          SizedBox(height: getProportionateScreenHeight(15)),
                          SizedBox(
                            height: getProportionateScreenHeight(40),
                            width: SizeConfig
                                .screenWidth, // Set the width as needed
                            child: ElevatedButton(
                              onPressed: () async {
                                try {
                                  if (_emailController.text.isEmpty) {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        content: Text("Email harus diisi"),
                                        backgroundColor: red,
                                      ),
                                    );
                                  } else if (!RegExp(emailRegex)
                                      .hasMatch(_emailController.text)) {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        content: Text("Email Invalid"),
                                        backgroundColor: red,
                                      ),
                                    );
                                  } else {
                                    ForgotModel forgot = ForgotModel(
                                      email: _emailController.text,
                                    );
                                    await context
                                        .read<UserProvider>()
                                        .forgotUser(forgot);
                                    // ignore: use_build_context_synchronously
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        content:
                                            Text("Link reset telah dikirim"),
                                        backgroundColor: Colors.green,
                                      ),
                                    );

                                    _emailController.text = "";
                                  }
                                } catch (e) {
                                  // ignore: use_build_context_synchronously
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(
                                          "Terjadi Kesalahan saat Login: $e"),
                                      backgroundColor: red,
                                    ),
                                  );
                                }
                              },
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: secondary),
                              child: const Text(
                                "Kirim Email",
                                style: TextStyle(
                                  color: black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
