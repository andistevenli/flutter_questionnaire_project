import 'package:flutter/material.dart';
import 'package:flutter_questionnaire_project/utils/constant/color.dart';
import 'package:flutter_questionnaire_project/utils/constant/text.dart';
import 'package:flutter_questionnaire_project/utils/responsive/size_config.dart';

class OpeningScreen extends StatelessWidget {
  const OpeningScreen({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: Container(
              padding: const EdgeInsets.all(40),
              child: Center(
                child: Image.asset(
                  "assets/images/questionnairy_logo.png",
                  // width: getProportionateScreenHeight(360),
                  // height: getProportionateScreenWidth(368),
                ),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(40),
            decoration: const BoxDecoration(
                color: primary005,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(50),
                    topRight: Radius.circular(50))),
            height: getProportionateScreenHeight(319),
            width: SizeConfig.screenWidth,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  openinTitle,
                  style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 24, color: white),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(5),
                ),
                const Text(
                  openingDescription,
                  style: TextStyle(fontSize: 13, color: white),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(25),
                ),
                Row(
                  children: [
                    SizedBox(
                      height: getProportionateScreenHeight(40),
                      width: getProportionateScreenWidth(120),
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/login_screen');
                        },
                        style: ElevatedButton.styleFrom(
                            backgroundColor: secondary),
                        child: const Text(
                          "Log In",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    const Spacer(),
                    SizedBox(
                      height: getProportionateScreenHeight(40),
                      width: getProportionateScreenWidth(120),
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/register_screen');
                        },
                        style: ElevatedButton.styleFrom(
                            backgroundColor: secondary),
                        child: const Text(
                          "Register",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
