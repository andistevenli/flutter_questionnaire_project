import 'package:flutter/material.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/user_model.dart';
import 'package:flutter_questionnaire_project/features/dashboard/dashboard_controller.dart';
import 'package:flutter_questionnaire_project/features/profile/profile_screen.dart';
import 'package:flutter_questionnaire_project/features/questionnaire/preparation_screen.dart';
import 'package:flutter_questionnaire_project/features/widgets/error_state_widget.dart';
import 'package:flutter_questionnaire_project/features/widgets/loading_state_widget.dart';
import 'package:flutter_questionnaire_project/utils/constant/color.dart';
import 'package:flutter_questionnaire_project/utils/constant/route.dart';
import 'package:flutter_questionnaire_project/utils/constant/text.dart';
import 'package:flutter_questionnaire_project/utils/constant/unit.dart';
import 'package:flutter_questionnaire_project/utils/responsive/size_config.dart';
import 'package:flutter_questionnaire_project/utils/extensions/white_space.dart';
import 'package:flutter_questionnaire_project/utils/state/finite_state.dart';
import 'package:provider/provider.dart';
import 'package:pie_chart/pie_chart.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({super.key});

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  late final DashboardController dashboardController;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      dashboardController =
          Provider.of<DashboardController>(context, listen: false);
      final DashboardArgument dashboardArgs =
          ModalRoute.of(context)!.settings.arguments as DashboardArgument;
      dashboardController.getResultInfo(dashboardArgs.userModel.npm);
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    DashboardArgument args =
        ModalRoute.of(context)!.settings.arguments as DashboardArgument;
    return Scaffold(
      backgroundColor: primary005,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(screenPadding),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: IconButton(
                  style: const ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(white),
                  ),
                  iconSize: getProportionateScreenWidth(40.0),
                  onPressed: () => Navigator.pushNamed(
                    context,
                    profileRoute,
                    arguments: ProfileArgument(userModel: args.userModel),
                  ),
                  icon: const Icon(
                    Icons.person,
                    color: secondary066,
                  ),
                ),
              ),
              38.0.dp,
              const SizedBox(
                height: 63,
                width: double.infinity,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      welcome,
                      style: TextStyle(
                        color: white,
                        fontWeight: FontWeight.normal,
                        fontSize: 20,
                      ),
                    ),
                    Text(
                      appName,
                      style: TextStyle(
                        color: white,
                        fontWeight: FontWeight.bold,
                        fontSize: 24,
                      ),
                    ),
                  ],
                ),
              ),
              66.0.dp,
              Container(
                alignment: Alignment.center,
                width: double.infinity,
                height: getProportionateScreenHeight(318.0),
                padding: const EdgeInsets.symmetric(
                  vertical: 20.0,
                  horizontal: 24.0,
                ),
                decoration: BoxDecoration(
                  color: white,
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Consumer<DashboardController>(
                  builder: (context, controller, _) {
                    if (controller.finiteState == FiniteState.loading) {
                      return const LoadingStateWidget();
                    } else if (controller.finiteState == FiniteState.failed) {
                      return const ErrorStateWidget();
                    } else {
                      if (controller.result == null) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(
                              width: double.infinity,
                              child: Text(
                                chartSectionTitle,
                                style: TextStyle(
                                  color: grey,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            65.0.dp,
                            Column(
                              children: [
                                const SizedBox(
                                  width: double.infinity,
                                  child: Text(
                                    chartSectionSubtitle,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                    ),
                                  ),
                                ),
                                13.0.dp,
                                Align(
                                  alignment: Alignment.center,
                                  child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      backgroundColor: secondary,
                                      elevation: 0.0,
                                      fixedSize: Size(
                                        getProportionateScreenWidth(148),
                                        getProportionateScreenHeight(42),
                                      ),
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                      ),
                                    ),
                                    onPressed: () => Navigator.pushNamed(
                                      context,
                                      preparationRoute,
                                      arguments: PreparationArgument(
                                          userModel: args.userModel),
                                    ),
                                    child: const Text(
                                      startButton,
                                      style: TextStyle(
                                        color: black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        );
                      } else {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const SizedBox(
                              width: double.infinity,
                              child: Text(
                                chartSectionTitle,
                                style: TextStyle(
                                  color: black,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            // 65.0.dp,
                            PieChart(
                              dataMap: {
                                'Puas':
                                    controller.result!.satisfactionPercentage,
                                'Tidak Puas': controller
                                    .result!.dissatisfactionPercentage,
                              },
                              colorList: const [
                                primary005,
                                secondary,
                              ],
                            ),
                            // 50.0.dp,
                            const Text(
                              chartSectionCaption,
                              style: TextStyle(
                                fontSize: 12,
                                color: black,
                              ),
                            ),
                          ],
                        );
                      }
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class DashboardArgument {
  final UserModel userModel;

  DashboardArgument({
    required this.userModel,
  });
}
