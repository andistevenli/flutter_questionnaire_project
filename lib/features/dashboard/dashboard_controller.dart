import 'package:flutter/material.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/result_model.dart';
import 'package:flutter_questionnaire_project/core/domain/use-case/dashboard_interactor.dart';
import 'package:flutter_questionnaire_project/utils/state/finite_state.dart';

class DashboardController with ChangeNotifier {
  final DashboardInteractor dashboardInteractor = DashboardInteractor();
  ResultModel? result;

  FiniteState _finiteState = FiniteState.initial;
  FiniteState get finiteState => _finiteState;

  void changeState(FiniteState finiteState) {
    _finiteState = finiteState;
    notifyListeners();
  }

  Future<void> getResultInfo(final String npm) async {
    try {
      changeState(FiniteState.loading);
      result = await dashboardInteractor.getResultInfo(npm);
      changeState(FiniteState.loaded);
    } catch (e) {
      changeState(FiniteState.failed);
      throw Exception(e);
    }
  }
}
