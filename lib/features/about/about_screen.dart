import 'package:flutter/material.dart';
import 'package:flutter_questionnaire_project/utils/constant/color.dart';
import 'package:flutter_questionnaire_project/utils/constant/text.dart';
import 'package:flutter_questionnaire_project/utils/constant/unit.dart';
import 'package:flutter_questionnaire_project/utils/extensions/white_space.dart';
import 'package:flutter_questionnaire_project/utils/responsive/size_config.dart';

class AboutScreen extends StatefulWidget {
  const AboutScreen({super.key});

  @override
  State<AboutScreen> createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(screenPadding),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: IconButton(
                  iconSize: getProportionateScreenWidth(24.0),
                  onPressed: () => Navigator.pop(context),
                  icon: const Icon(
                    Icons.arrow_back,
                    color: black,
                  ),
                ),
              ),
              21.0.dp,
              Expanded(
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/images/questionnairy_logo.png',
                        width: getProportionateScreenWidth(238.0),
                        height: getProportionateScreenHeight(192.0),
                      ),
                      21.0.dp,
                      const Text(
                        appName,
                        style: TextStyle(
                          color: black,
                          fontWeight: FontWeight.bold,
                          fontSize: 32,
                        ),
                      ),
                      16.0.dp,
                      const Text(
                        version,
                        style: TextStyle(color: black, fontSize: 16),
                      ),
                      21.0.dp,
                      const Divider(
                        color: primary005,
                        thickness: 1,
                      ),
                      36.0.dp,
                      const Text(
                        appDesc,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: black,
                          fontSize: 14,
                        ),
                      ),
                      21.0.dp,
                      const Text(
                        'Oleh',
                        style: TextStyle(color: black, fontSize: 16),
                      ),
                      const Text(
                        author,
                        style: TextStyle(
                          color: black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                      ),
                      12.0.dp,
                      Image.asset(
                        'assets/images/universitas_nasional_logo.png',
                        width: getProportionateScreenWidth(56.0),
                        height: getProportionateScreenHeight(74.0),
                      ),
                      27.0.dp,
                      const Text(
                        authorMajor,
                        style: TextStyle(color: black, fontSize: 16),
                      ),
                      const Text(
                        authorCollege,
                        style: TextStyle(
                            color: black,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
