import 'package:flutter/material.dart';
import 'package:flutter_questionnaire_project/utils/responsive/size_config.dart';

extension Space on double {
  SizedBox get dp => SizedBox(
        height: getProportionateScreenHeight(this),
      );
}
