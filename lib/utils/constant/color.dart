import 'package:flutter/material.dart';

const Color primary005 = Color(0xFF005489);
const Color secondary = Color(0xFFFFB100);
const Color white = Color(0xFFFFFFFF);
const Color black = Color(0xFF000000);
const Color primary478 = Color(0xFF478CB8);
const Color red = Color(0xFFDD0606);
const Color grey = Color(0xFF656565);
const Color secondary066 = Color(0xFFFFD066);
