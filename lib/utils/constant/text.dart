const String projectName = 'Flutter Questionnaire Project';
const String welcome = 'Selamat Datang di';
const String appName = 'Questionnairy';
const String chartSectionTitle = 'Analisis Kepuasan Pengguna';
const String chartSectionCaption = 'Note: Kuesioner telah terisi';
const String chartSectionSubtitle = 'Kuesioner Belum Dikerjakan';
const String startButton = 'Mulai';
const String profile = 'Profil Pengguna';
const String fullName = 'Nama Lengkap';
const String idNumber = 'NPM/NIM';
const String major = 'Jurusan';
const String advisor = 'Dosen Pembimbing';
const String email = 'Alamat Email';
const String more = 'Lainnya';
const String about = 'About';
const String logout = 'Logout';
const String confirmQuestion = 'Apakah Anda yakin ingin keluar dari akun Anda?';
const String yes = 'Ya';
const String no = 'Tidak';
const String version = 'V.1.0';
const String appDesc =
    'Pengembangan Aplikasi Android untuk kuesioner dengan menggunakan algoritma C4.5 untuk menganalisis hasil kuesioner';
const String author = 'Muhammad Rayhan Putra';
const String authorMajor = 'Program Studi Teknik Informatika';
const String authorCollege = 'Universitas Nasional';
const String openingDescription =
    "Selamat datang di Aplikasi Questionnairy! Kami sangat senang Anda bergabung. Berikan pandangan dan tanggapan Anda yang berharga untuk membantu kita memahami lebih baik dan membuat perubahan yang positif. Terima kasih atas partisipasinya!";
const openinTitle = "Selamat Datang";
const forgotDescription =
    "Ups! Tampaknya Anda lupa kata sandi. Mohon masukkan alamat email Anda untuk menerima tautan reset kata sandi";
const forgotPassword = "Lupa Kata Sandi?";
const String questionnaire = 'Kuesioner';
const String clueTitle = 'PETUNJUK';
const String clueContent =
    'Jawablah 25 pertanyaan dengan memilih salah satu dari alternatif jawaban yang telah di sediakan.';
const String confirmSubmit =
    'Sebelum melanjutkan, pastikan semua data telah diisi dengan benar. Yakin ingin melanjutkan?';
const String back = 'Kembali';
const String sure = 'Yakin';
const String submitButton = 'Submit';
