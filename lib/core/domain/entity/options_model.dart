class OptionsModel {
  final int value;
  final String content;

  OptionsModel({
    required this.value,
    required this.content,
  });
}
