class UserModel {
  final String name;
  final String npm;
  final String jurusan;
  final String dospem;
  final String email;
  final String password;

  UserModel({
    required this.name,
    required this.npm,
    required this.jurusan,
    required this.dospem,
    required this.email,
    required this.password,
  });

  factory UserModel.fromJSON(Map<dynamic, dynamic> json) {
    return UserModel(
      name: json['name'].toString(),
      npm: json['npm'].toString(),
      jurusan: json['jurusan'].toString(),
      dospem: json['dospem'].toString(),
      email: json['email'].toString(),
      password: json['password'].toString(),
    );
  }
}
