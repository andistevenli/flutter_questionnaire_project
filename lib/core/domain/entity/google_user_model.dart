class GoogleUserModel {
  final String name;
  final String email;

  GoogleUserModel({
    required this.name,
    required this.email,
  });
}
