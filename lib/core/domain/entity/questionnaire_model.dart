import 'package:flutter_questionnaire_project/core/domain/entity/options_model.dart';

class QuestionnaireModel {
  final int number;
  final String question;
  final List<OptionsModel> optionsModel;

  QuestionnaireModel({
    required this.number,
    required this.question,
    required this.optionsModel,
  });
}
