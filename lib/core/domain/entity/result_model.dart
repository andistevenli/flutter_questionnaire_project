class ResultModel {
  final String npm;
  final String respondentName;
  final double satisfactionPercentage;
  final double dissatisfactionPercentage;
  final List<int> answersValue;
  final List<String> answersLabel;

  ResultModel({
    required this.npm,
    required this.respondentName,
    required this.satisfactionPercentage,
    required this.dissatisfactionPercentage,
    required this.answersValue,
    required this.answersLabel,
  });
}
