import 'package:flutter_questionnaire_project/core/data/dto/questionnaire_dto.dart';
import 'package:flutter_questionnaire_project/core/data/mapper/questionnaire_mapper.dart';
import 'package:flutter_questionnaire_project/core/data/mapper/result_mapper.dart';
import 'package:flutter_questionnaire_project/core/data/repo/questionnaire_repo.dart';
import 'package:flutter_questionnaire_project/core/data/source/local/local_data_source.dart';
import 'package:flutter_questionnaire_project/core/data/source/remote/remote_data_source.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/questionnaire_model.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/result_model.dart';

class QuestionnaireRepoImpl implements IQuestionnaireRepo {
  LocalDataSource localDataSource = LocalDataSource();
  RemoteDataSource remoteDataSource = RemoteDataSource();

  @override
  Future<void> postResult(ResultModel resultModel) async {
    try {
      await remoteDataSource.postResult(moveToResultDTO(resultModel));
    } catch (e) {
      throw Exception(e);
    }
  }

  @override
  Future<List<QuestionnaireModel>> getQuestionnaireInfo() async {
    try {
      final List<QuestionnaireDTO> questionnaireDTO =
          await localDataSource.getQuestionnaireDTO();
      return questionnaireDTO.map((e) => moveToQuestionnaireModel(e)).toList();
    } catch (e) {
      throw Exception(e);
    }
  }
}
