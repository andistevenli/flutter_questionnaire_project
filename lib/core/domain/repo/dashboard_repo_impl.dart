import 'package:flutter_questionnaire_project/core/data/mapper/result_mapper.dart';
import 'package:flutter_questionnaire_project/core/data/repo/dashboard_repo.dart';
import 'package:flutter_questionnaire_project/core/data/source/remote/remote_data_source.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/result_model.dart';

class DashboardRepoImpl implements IDashboardRepo {
  RemoteDataSource remoteDataSource = RemoteDataSource();

  @override
  Future<ResultModel?> getResultInfo(final String npm) async {
    try {
      return moveToResultModel(await remoteDataSource.getResultInfo(npm));
    } catch (e) {
      throw Exception(e);
    }
  }
}
