import 'package:flutter_questionnaire_project/core/data/repo/profile_repo.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/user_model.dart';
import 'package:flutter_questionnaire_project/core/data/source/remote/firebase/firebase_services.dart';

class ProfileRepoImpl implements IProfileRepo {
  FirebaseService firebaseService = FirebaseService();

  @override
  Future<UserModel> getProfileInfo(final String npm) async {
    try {
      return await firebaseService.getProfileInfo(npm);
    } catch (e) {
      throw Exception(e);
    }
  }

  @override
  Future<void> userLogout() async {
    try {
      await firebaseService.userLogout();
    } catch (e) {
      throw Exception(e);
    }
  }
}
