import 'package:flutter_questionnaire_project/core/domain/entity/user_model.dart';
import 'package:flutter_questionnaire_project/core/domain/repo/profile_repo_impl.dart';
import 'package:flutter_questionnaire_project/core/domain/use-case/profile_use_case.dart';

class ProfileInteractor implements ProfileUseCase {
  ProfileRepoImpl profileRepoImpl = ProfileRepoImpl();

  @override
  Future<UserModel> getProfileInfo(String npm) async {
    try {
      return await profileRepoImpl.getProfileInfo(npm);
    } catch (e) {
      throw Exception(e);
    }
  }

  @override
  Future<void> userLogout() async {
    try {
      await profileRepoImpl.userLogout();
    } catch (e) {
      throw Exception(e);
    }
  }
}
