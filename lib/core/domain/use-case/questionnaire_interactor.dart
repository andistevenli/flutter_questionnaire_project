import 'package:flutter_questionnaire_project/core/domain/entity/questionnaire_model.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/result_model.dart';
import 'package:flutter_questionnaire_project/core/domain/repo/questionnaire_repo_impl.dart';
import 'package:flutter_questionnaire_project/core/domain/use-case/questionnaire_use_case.dart';

class QuestionnaireInteractor implements QuestionnaireUseCase {
  QuestionnaireRepoImpl questionnaireRepoImpl = QuestionnaireRepoImpl();

  @override
  Future<void> postResult(ResultModel resultModel) async {
    try {
      await questionnaireRepoImpl.postResult(resultModel);
    } catch (e) {
      throw Exception(e);
    }
  }

  @override
  Future<List<QuestionnaireModel>> getQuestionnaireInfo() async {
    try {
      return await questionnaireRepoImpl.getQuestionnaireInfo();
    } catch (e) {
      throw Exception(e);
    }
  }
}
