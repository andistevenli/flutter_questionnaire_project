import 'package:flutter_questionnaire_project/core/domain/entity/result_model.dart';
import 'package:flutter_questionnaire_project/core/domain/repo/dashboard_repo_impl.dart';
import 'package:flutter_questionnaire_project/core/domain/use-case/dashboard_use_case.dart';

class DashboardInteractor implements DashboardUseCase {
  DashboardRepoImpl dashboardRepoImpl = DashboardRepoImpl();

  @override
  Future<ResultModel?> getResultInfo(final String npm) async {
    try {
      return await dashboardRepoImpl.getResultInfo(npm);
    } catch (e) {
      throw Exception(e);
    }
  }
}
