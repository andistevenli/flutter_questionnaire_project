import 'package:flutter_questionnaire_project/core/domain/entity/result_model.dart';

abstract class DashboardUseCase {
  Future<ResultModel?> getResultInfo(final String npm);
}
