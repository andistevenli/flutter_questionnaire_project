import 'package:flutter_questionnaire_project/core/domain/entity/user_model.dart';

abstract class ProfileUseCase {
  Future<UserModel> getProfileInfo(final String npm);
  Future<void> userLogout();
}
