class ResultDTO {
  final String npm;
  final String respondentName;
  final double satisfactionPercentage;
  final double dissatisfactionPercentage;
  final List<int> answersValue;
  final List<String> answersLabel;

  ResultDTO({
    required this.npm,
    required this.respondentName,
    required this.satisfactionPercentage,
    required this.dissatisfactionPercentage,
    required this.answersValue,
    required this.answersLabel,
  });

  factory ResultDTO.fromJSON(Map<dynamic, dynamic> json) {
    final List<int> answersValue = [];
    final List<String> answersLabel = [];
    for(var value in json['answers_value']){
      answersValue.add(int.parse(value.toString()));
    }
    for(var label in json['answers_label']){
      answersLabel.add(label.toString());
    }
    return ResultDTO(
      npm: json['npm'].toString(),
      respondentName: json['respondent_name'].toString(),
      satisfactionPercentage:
          double.parse(json['satisfaction_percentage'].toString()),
      dissatisfactionPercentage:
          double.parse(json['dissatisfaction_percentage'].toString()),
      answersValue: answersValue,
      answersLabel: answersLabel,
    );
  }
}
