class OptionsDTO {
  final int value;
  final String content;

  OptionsDTO({
    required this.value,
    required this.content,
  });

  factory OptionsDTO.fromJSON(Map<String, dynamic> json) {
    return OptionsDTO(
      value: json['value'],
      content: json['content'],
    );
  }
}
