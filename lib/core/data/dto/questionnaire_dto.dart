import 'package:flutter_questionnaire_project/core/data/dto/options_dto.dart';

class QuestionnaireDTO {
  final int number;
  final String question;
  final List<OptionsDTO> optionsDTO;

  QuestionnaireDTO({
    required this.number,
    required this.question,
    required this.optionsDTO,
  });

  factory QuestionnaireDTO.fromJSON(Map<String, dynamic> json) {
    List<OptionsDTO> options = [];
    if (json['options'] != null) {
      for (var option in json['options']) {
        options.add(OptionsDTO.fromJSON(option));
      }
    }
    return QuestionnaireDTO(
      number: json['number'],
      question: json['question'],
      optionsDTO: options,
    );
  }
}
