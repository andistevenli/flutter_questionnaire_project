import 'package:flutter_questionnaire_project/core/data/dto/options_dto.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/options_model.dart';

OptionsModel moveToOptionsModel(OptionsDTO from) {
  return OptionsModel(
    value: from.value,
    content: from.content,
  );
}
