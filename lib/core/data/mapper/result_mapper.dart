import 'package:flutter_questionnaire_project/core/data/dto/result_dto.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/result_model.dart';

ResultModel? moveToResultModel(ResultDTO? from) {
  if (from != null) {
    return ResultModel(
      npm: from.npm,
      respondentName: from.respondentName,
      satisfactionPercentage: from.satisfactionPercentage,
      dissatisfactionPercentage: from.dissatisfactionPercentage,
      answersValue: from.answersValue,
      answersLabel: from.answersLabel,
    );
  } else {
    return null;
  }
}

ResultDTO moveToResultDTO(ResultModel from) {
  return ResultDTO(
    npm: from.npm,
    respondentName: from.respondentName,
    satisfactionPercentage: from.satisfactionPercentage,
    dissatisfactionPercentage: from.dissatisfactionPercentage,
    answersValue: from.answersValue,
    answersLabel: from.answersLabel,
  );
}
