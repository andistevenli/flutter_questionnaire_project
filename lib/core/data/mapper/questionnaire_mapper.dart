import 'package:flutter_questionnaire_project/core/data/dto/questionnaire_dto.dart';
import 'package:flutter_questionnaire_project/core/data/mapper/options_mapper.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/options_model.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/questionnaire_model.dart';

QuestionnaireModel moveToQuestionnaireModel(QuestionnaireDTO from) {
  List<OptionsModel> options = [];
  for (var option in from.optionsDTO) {
    options.add(moveToOptionsModel(option));
  }
  return QuestionnaireModel(
    number: from.number,
    question: from.question,
    optionsModel: options,
  );
}
