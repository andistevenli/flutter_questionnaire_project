import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:flutter_questionnaire_project/core/data/dto/questionnaire_dto.dart';
import 'package:flutter_questionnaire_project/utils/constant/path.dart';

class LocalDataSource {
  Future<List<QuestionnaireDTO>> getQuestionnaireDTO() async {
    try {
      dynamic response = jsonDecode(await rootBundle.loadString(jsonFilePath));
      return (response as List)
          .map((e) => QuestionnaireDTO.fromJSON(e))
          .toList();
    } catch (e) {
      throw Exception(e);
    }
  }
}
