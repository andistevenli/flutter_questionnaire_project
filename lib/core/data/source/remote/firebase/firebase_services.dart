import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter_questionnaire_project/core/data/dto/result_dto.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/forgot_model.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/login_model.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/user_model.dart';

class FirebaseService {
  final DatabaseReference _usersRef =
      FirebaseDatabase.instance.ref().child('users');
  final DatabaseReference _resultRef =
      FirebaseDatabase.instance.ref().child('results');
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  Future<void> userLogout() async {
    try {
      await firebaseAuth.signOut();
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<void> postResult(ResultDTO resultDTO) async {
    try {
      await _resultRef.child(resultDTO.npm).set({
        'npm': resultDTO.npm,
        'respondent_name': resultDTO.respondentName,
        'satisfaction_percentage': resultDTO.satisfactionPercentage,
        'dissatisfaction_percentage': resultDTO.dissatisfactionPercentage,
        'answers_value': resultDTO.answersValue,
        'answers_label': resultDTO.answersLabel,
      });
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<UserModel> getProfileInfo(final String npm) async {
    try {
      final DataSnapshot snapshot = await _usersRef.child(npm).get();
      if (snapshot.exists) {
        return UserModel.fromJSON(snapshot.value as Map<dynamic, dynamic>);
      } else {
        return UserModel(
          name: '-',
          npm: '-',
          jurusan: '-',
          dospem: '-',
          email: '-',
          password: '-',
        );
      }
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<UserModel> getUserInfo(final String email) async {
    try {
      final DataSnapshot snapshot = await _usersRef.get();
      late UserModel userModel;
      if (snapshot.exists) {
        for (var user in snapshot.children) {
          for (var userData in user.children) {
            if (userData.value == email) {
              userModel =
                  UserModel.fromJSON(user.value as Map<dynamic, dynamic>);
            }
          }
        }
      }
      return userModel;
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<ResultDTO?> getResultInfo(final String npm) async {
    try {
      final DataSnapshot snapshot = await _resultRef.child(npm).get();
      if (snapshot.exists) {
        return ResultDTO.fromJSON(snapshot.value as Map<dynamic, dynamic>);
      } else {
        return null;
      }
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<void> userSignInFirebase(UserModel user) async {
    try {
      await firebaseAuth.createUserWithEmailAndPassword(
          email: user.email, password: user.password);
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<void> addUserToFirebase(UserModel user) async {
    try {
      await _usersRef.child(user.npm).set({
        'name': user.name,
        'npm': user.npm,
        'jurusan': user.jurusan,
        'dospem': user.dospem,
        'email': user.email,
      });
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<void> userLoginFirebase(LoginModel login) async {
    try {
      await firebaseAuth.signInWithEmailAndPassword(
          email: login.email, password: login.password);
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<void> userForgotFirebase(ForgotModel forgot) async {
    try {
      await firebaseAuth.sendPasswordResetEmail(email: forgot.email);
    } catch (e) {
      throw Exception(e);
    }
  }
}
