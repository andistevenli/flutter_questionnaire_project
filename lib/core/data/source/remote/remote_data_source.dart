import 'package:flutter_questionnaire_project/core/data/dto/result_dto.dart';
import 'package:flutter_questionnaire_project/core/data/source/remote/firebase/firebase_services.dart';

class RemoteDataSource {
  FirebaseService firebaseService = FirebaseService();

  Future<void> postResult(ResultDTO resultDTO) async {
    try {
      await firebaseService.postResult(resultDTO);
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<ResultDTO?> getResultInfo(final String npm) async {
    try {
      return await firebaseService.getResultInfo(npm);
    } catch (e) {
      throw Exception(e);
    }
  }
}
