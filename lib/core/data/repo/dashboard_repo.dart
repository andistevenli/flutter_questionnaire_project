import 'package:flutter_questionnaire_project/core/domain/entity/result_model.dart';

abstract class IDashboardRepo {
  Future<ResultModel?> getResultInfo(final String npm);
}
