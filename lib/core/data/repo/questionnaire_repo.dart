import 'package:flutter_questionnaire_project/core/domain/entity/questionnaire_model.dart';
import 'package:flutter_questionnaire_project/core/domain/entity/result_model.dart';

abstract class IQuestionnaireRepo {
  Future<void> postResult(ResultModel resultModel);
  Future<List<QuestionnaireModel>> getQuestionnaireInfo();
}
